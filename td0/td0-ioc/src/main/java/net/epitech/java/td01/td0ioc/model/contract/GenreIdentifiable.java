package net.epitech.java.td01.td0ioc.model.contract;

import net.epitech.java.td01.td0ioc.model.types.SexType;

/**
 * Qualifies an entity that has a sex
 * @author nicolas
 *
 */
public interface GenreIdentifiable {
	
	public abstract SexType getSex();

}
