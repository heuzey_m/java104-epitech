package eu.epitech.java;

import com.google.common.base.Predicate;
import com.google.common.collect.Range;

/**
 * this interface provide contract for a concrete class to print a range of
 * number filtered by a predicate
 * 
 * @author nicolas
 * 
 * @param <T>
 */
public interface NumberPrinter<T extends Comparable<?>> {

	/**
	 * 
	 * @param rng
	 *            a range to be filtered and displayed
	 * @param selector
	 *            a predicate to filter the range
	 */
	public abstract void doPrint(Range<T> rng, Predicate<T> selector);

}