package net.epitech.java.td.exception;

public class NoSuchCourseException extends Exception {

	public NoSuchCourseException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -307687134199634167L;

}
