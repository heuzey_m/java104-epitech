package net.epitech.java.td.exception;

public class NoSuchTeacherException extends Exception {

	public NoSuchTeacherException(String message) {
		super(message);
	}

}
