<%@ include file="menu-header.jsp"%>

<form:form method="post" action="${webappRoot}/teacher"
	commandName="teacher">
	<div class="form-group">

		<form:label class="form-control" path="name">name</form:label>
		<form:input path="name" />
		<form:errors path="name" cssClass="error" />
		<br />
		<form:label class="form-control" path="email">email</form:label>
		<form:input path="email" />
		<form:errors path="email" cssClass="error" />
		<br />
		<form:label class="form-control" path="password">password</form:label>
		<form:password path="password" />
		<form:errors path="password" cssClass="error" />
		<br /> <br />
		<form:select path="cours">
			<form:option value="NONE" label="--- Select ---" />
			<c:forEach var="selectCourse" items="${courses}">
				<form:option value="${selectCourse.id}">
					<c:out value="${selectCourse.name}" />
				</form:option>
			</c:forEach>
		</form:select>
		<br /> <br /> <input type="submit" class="btn btn-default"
			value="Add Onizuka Teacher" />
	</div>

	<input type="hidden" name="${_csrf.parameterName}"
		value="${_csrf.token}" />

</form:form>


<%@ include file="menu-footer.jsp"%>