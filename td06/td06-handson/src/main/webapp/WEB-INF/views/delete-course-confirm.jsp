<%@ include file="menu-header.jsp" %>

	Are you sure you want to delete
	<c:out value="${course.name}" />
	course ?

	<c:url value="delete-course-confirmed" var="deletecourseUrl">
		<c:param name="courseId" value="${course.id}"></c:param>
	</c:url>
	<a href="${deletecourseUrl}"><button class="button">Yes</button></a>
	<a href="${webappRoot}"><button class="button">Cancel</button></a>

<%@ include file="menu-footer.jsp" %>
