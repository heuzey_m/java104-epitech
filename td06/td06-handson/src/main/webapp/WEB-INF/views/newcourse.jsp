<%@ include file="menu-header.jsp" %>

	<form:form method="post" action="${webappRoot}/course"
		commandName="course">
		<div class="form-group">

			<form:label class="form-control" path="name">name</form:label>
			<form:input path="name" />
			<form:errors path="name" cssClass="error" />
			<br />
			<form:label class="form-control" path="dayInweek">dayInweek</form:label>
			<form:input path="dayInweek" />
			<form:errors path="dayInweek" cssClass="error" />
			<br />
			<form:label class="form-control" path="duration">duration</form:label>
			<form:input path="duration" />
			<form:errors path="duration" cssClass="error" />


			<br /> <input type="submit" class="btn btn-default"
				value="Add Cours" />
		</div>

		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form:form>

	
	<%@ include file="menu-footer.jsp" %>